﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerBall : PathNPCAgent
{
    protected float nextPathCheckTime = 0f;
    protected Bounds startRoomArea;

    protected override void Start()
    {
        base.Start();
        stateActions.Add(State.Idle, idle);
        stateActions.Add(State.FollowPlayer, followPlayer);
        stateActions.Add(State.MoveToPlayerLeftClickPosition, moveToPlayerLeftClickPosition);
        stateActions.Add(State.ReturnToStartRoom, returnToStartRoom);

        Dictionary<State, List<State>> specification = new Dictionary<State, List<State>>();
        specification.Add(State.FollowPlayer, new List<State>() 
        { 
            State.Same, 
            State.Idle,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.ReturnToStartRoom,
            State.Same,
            State.Idle,
            State.Same,
            State.Same,
            State.MoveToPlayerLeftClickPosition
        });
        specification.Add(State.Idle, new List<State>() 
        { 
            State.FollowPlayer,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.ReturnToStartRoom,
            State.FollowPlayer,
            State.Same,
            State.Same,
            State.Same,
            State.MoveToPlayerLeftClickPosition
        });
        specification.Add(State.MoveToPlayerLeftClickPosition, new List<State>() 
        { 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.ReturnToStartRoom,
            State.FollowPlayer,
            State.Idle,
            State.Same,
            State.Same,
            State.Same
        });
        specification.Add(State.ReturnToStartRoom, new List<State>() 
        { 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.FollowPlayer,
            State.Idle,
            State.Same,
            State.Same,
            State.MoveToPlayerLeftClickPosition
        });
        stateMachine = new StateMachine(specification);

        startRoomArea = GameObject.Find("StartRoomArea").collider.bounds;
    }

    protected virtual void followPlayer()
    {
        if (notCloseEnoughWhenFollowing()
            && Time.time > nextPathCheckTime)
        {
            nextPathCheckTime = Time.time + 0.5f;
            CalculatePathTo(gameManager.Player.transform.position);
        }
        followPath();
    }

    protected bool notCloseEnoughWhenFollowing()
    {
        return Vector3.Distance(this.transform.position, gameManager.Player.transform.position) 
            >= 2 * this.collider.bounds.size.x;
    }

    protected virtual void moveToPlayerLeftClickPosition()
    {
        CalculatePathTo(gameManager.PlayerLeftClickPosition);
        followPath();
        if (pathManager.GetPath().Count <= 0)
        {
            gameManager.HandleTriggerMessage(11);
        }
    }

    protected virtual void returnToStartRoom()
    {
        if (!this.collider.bounds.Intersects(startRoomArea))
        {
            CalculatePathTo(startRoomArea.center);
            followPath();
        }
        else
        {
            if (gameManager.Player.collider.bounds.Intersects(startRoomArea))
            {
                gameManager.HandleTriggerMessage(12);
            }
            else
            {
                gameManager.HandleTriggerMessage(13);
            }
        }
    }
}
