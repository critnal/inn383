﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class BouncingShape : NPCAgent
{
    protected Vector3 storedVelocity;
    protected Vector3 storedAngularVelocity;
    protected readonly float CLOSE_BOUNCE_DISTANCE = 2f;
    protected readonly float FAR_BOUNCE_DISTANCE = 4f;

    protected override void Start()
    {
        base.Start();

        //stateActions.Add(State.Bounce, this.bounce);
        //stateActions.Add(State.Freeze, this.freeze);

        Dictionary<State, List<State>> specification = new Dictionary<State, List<State>>();
        specification.Add(State.Bounce, new List<State>() 
        { 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Freeze,
            State.BounceCloseToPlayer,
            State.BounceAwayFromPlayer,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same

        });
        specification.Add(State.Freeze, new List<State>() 
        { 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Bounce,
            State.Same,
            State.BounceCloseToPlayer,
            State.BounceAwayFromPlayer,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same

        });
        specification.Add(State.BounceCloseToPlayer, new List<State>() 
        { 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Bounce,
            State.Freeze,
            State.Same,
            State.BounceAwayFromPlayer,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same

        });
        stateMachine = new StateMachine(specification);
        specification.Add(State.BounceAwayFromPlayer, new List<State>() 
        { 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Bounce,
            State.Freeze,
            State.BounceCloseToPlayer,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same

        });
        stateMachine = new StateMachine(specification);

        this.rigidbody.velocity = new Vector3(Random.Range(-1f, 1f), Random.Range(-1f, 1f), Random.Range(-1f, 1f));
        this.rigidbody.velocity *= MoveSpeed;
    }

    protected override void Update()
    {

    }

    protected override void FixedUpdate()
    {
        if (stateMachine.State == State.Freeze)
        {
            if (this.rigidbody.velocity.sqrMagnitude > 0)
            {
                storedVelocity = this.rigidbody.velocity;
                this.rigidbody.velocity = Vector3.zero;
                storedAngularVelocity = this.rigidbody.angularVelocity;
                this.rigidbody.angularVelocity = Vector3.zero;
            }
        }
        else
        {
            if (this.rigidbody.velocity.sqrMagnitude == 0)
            {
                this.rigidbody.velocity = storedVelocity;
                this.rigidbody.angularVelocity = storedAngularVelocity;
            }
        }

        if (stateMachine.State == State.BounceCloseToPlayer)
        {
            if (Vector3.Distance(this.transform.position, gameManager.Player.transform.position) 
                > CLOSE_BOUNCE_DISTANCE)
            {
                Vector3 directionTowardPlayer = gameManager.Player.transform.position - this.transform.position;
                this.rigidbody.velocity = directionTowardPlayer.normalized * MoveSpeed;
            }
        }
        
        if (stateMachine.State == State.BounceAwayFromPlayer)
        {
            if (Vector3.Distance(this.transform.position, gameManager.Player.transform.position)
                < FAR_BOUNCE_DISTANCE)
            {
                Vector3 directionAwayFromPlayer = this.transform.position - gameManager.Player.transform.position;
                this.rigidbody.velocity = directionAwayFromPlayer.normalized * MoveSpeed;
            }
        }
    }

    protected virtual void OnCollisionExit(Collision collision)
    {
        this.rigidbody.velocity = this.rigidbody.velocity.normalized * MoveSpeed;
    }
}
