﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HidingBall : PathNPCAgent
{
    protected float nextPathCheckTime = 0f;
    protected Bounds patrolArea;
    protected List<GameObject> hidingObjects;

    protected override void Start()
    {
        base.Start();

        MoveSpeed = 8f;

        stateActions.Add(State.Idle, idle);
        stateActions.Add(State.Patrol, patrol);
        stateActions.Add(State.Hide, hide);

        Dictionary<State, List<State>> specification = new Dictionary<State, List<State>>();
        specification.Add(State.Idle, new List<State>() 
        { 
            State.Same, 
            State.Same,
            State.Hide,
            State.Patrol,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same
        });
        specification.Add(State.Patrol, new List<State>() 
        { 
            State.Idle, 
            State.Idle,
            State.Hide,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same
        });
        specification.Add(State.Hide, new List<State>() 
        { 
            State.Idle, 
            State.Idle,
            State.Same,
            State.Patrol,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same
        });
        stateMachine = new StateMachine(specification);

        patrolArea = GameObject.Find("HidingBallPatrolArea").collider.bounds;
        hidingObjects = new List<GameObject>();
        foreach (GameObject gmObject in GameObject.FindGameObjectsWithTag("PathObstacle"))
        {
            if (gmObject.collider.bounds.Intersects(patrolArea) 
                && gmObject.name == "cylindrical Glass")
            {
                hidingObjects.Add(gmObject);
                print(gmObject.name);
            }
        }
    }

    protected override void patrol()
    {
        if (pathManager.GetPath().Count > 0)
        {
            followPath();
        }
        else
        {
            Vector3 randomPatrolPoint = getRandomPointWithinBounds(patrolArea);
            CalculatePathTo(randomPatrolPoint);
        }
    }

    protected Vector3 getRandomPointWithinBounds(Bounds bounds)
    {
        float x = Random.Range(bounds.min.x, bounds.max.x);
        float y = Random.Range(bounds.min.y, bounds.max.y);
        float z = Random.Range(bounds.min.z, bounds.max.z);
        return new Vector3(x, y, z);
    }

    protected override void hide()
    {
        this.rigidbody.velocity = Vector3.zero;
        Vector3 playerPosition = gameManager.Player.transform.position;

        if (pathManager.GetPath().Count > 0)
        {
            PositionToMoveTo = pathManager.GetNextPositionToMoveTo();
            MoveToward(PositionToMoveTo);
        }
        else if (canSeePlayer(playerPosition))
        {
            Dictionary<GameObject, float> hidingObjectDistancesFromMe
                = getGameObjectDistanceDictionaryFromPosition(hidingObjects, this.transform.position);
            Dictionary<GameObject, float> hidingObjectDistancesFromPlayer
                = getGameObjectDistanceDictionaryFromPosition(hidingObjects, playerPosition);

            GameObject bestHidingObject =
                getBestHidingObject(hidingObjectDistancesFromMe, hidingObjectDistancesFromPlayer);
            Vector3 hidingPosition = getHidingPositionOnGameObject(bestHidingObject, playerPosition);
            CalculatePathTo(hidingPosition);
        }
        else
        {
            HaltMovement();
        }
    }

    protected virtual bool canSeePlayer(Vector3 playerPosition)
    {
        RaycastHit hit;
        if (Physics.Raycast(this.transform.position,
            playerPosition - this.transform.position, out hit))
        {
            if (hit.collider.gameObject == gameManager.Player)
            {
                return true;
            }
        }
        return false;
    }

    protected virtual Dictionary<GameObject, float> getGameObjectDistanceDictionaryFromPosition(
        List<GameObject> gameObjects, Vector3 position)
    {
        Dictionary<GameObject, float> objectDistances = new Dictionary<GameObject, float>();
        foreach (GameObject gameObject in gameObjects)
        {
            float distance = Vector3.Distance(gameObject.transform.position, position);
            objectDistances.Add(gameObject, distance);
        }
        return objectDistances;
    }

    protected virtual GameObject getBestHidingObject(
        Dictionary<GameObject, float> hidingObjectDistancesFromMe,
        Dictionary<GameObject, float> hidingObjectDistancesFromPlayer)
    {
        GameObject bestHidingObject = getClosestHidingObjectFromPosition(
            hidingObjectDistancesFromMe, this.transform.position);
        float furthestDistanceFromPlayer = 0f;
        foreach (GameObject key in hidingObjectDistancesFromMe.Keys)
        {
            if (hidingObjectDistancesFromPlayer[key] > furthestDistanceFromPlayer
                && hidingObjectDistancesFromPlayer[key] >= hidingObjectDistancesFromMe[key])
            {
                bestHidingObject = key;
                furthestDistanceFromPlayer = hidingObjectDistancesFromPlayer[key];
            }
        }
        return bestHidingObject;
    }

    protected virtual GameObject getClosestHidingObjectFromPosition(
        Dictionary<GameObject, float> hidingObjectDistances, Vector3 position)
    {
        float closestDistance = 100000000f;
        GameObject closestHidingObject = null;
        foreach (GameObject key in hidingObjectDistances.Keys)
        {
            if (hidingObjectDistances[key] < closestDistance)
            {
                closestHidingObject = key;
                closestDistance = hidingObjectDistances[key];
            }
        }
        return closestHidingObject;
    }

    protected virtual Vector3 getHidingPositionOnGameObject(
        GameObject hidingObject, Vector3 playerPosition)
    {
        Vector3 playerDirectionFromObject 
            = Vector3.Normalize(playerPosition - hidingObject.transform.position);
        Vector3 localPositionOnObjectOppositePlayer 
            = (-1 * playerDirectionFromObject) * (hidingObject.transform.localScale.x / 2);
        return hidingObject.transform.position + localPositionOnObjectOppositePlayer;
    }
}
