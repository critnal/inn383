﻿using UnityEngine;
using System.Collections;
using System.Text.RegularExpressions;

public sealed class Trigger : MonoBehaviour
{
    private int triggerIndex = 0;
    private GameManager gameManager;

    void Start()
    {
        string indexString = this.name.Replace("Trigger", "");
        int.TryParse(indexString, out triggerIndex);
        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.name == "Player")
        {
            gameManager.HandleTriggerMessage(triggerIndex);
        }
    }
}
