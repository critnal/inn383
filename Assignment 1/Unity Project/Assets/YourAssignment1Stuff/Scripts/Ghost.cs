﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Ghost : NPCAgent
{
    protected Vector3 spawnPosition;
    protected float nextPathCheckTime;

    protected override void Start()
    {
        base.Start();

        MoveSpeed = 1.5f;

        stateActions.Add(State.Hibernate, hibernate);
        stateActions.Add(State.Idle, idle);
        stateActions.Add(State.FollowPlayer, followPlayer);
        stateActions.Add(State.Hide, hide);
        stateActions.Add(State.SurprisePlayerFromBehind, surprisePlayerFromBehind);

        Dictionary<State, List<State>> specification = new Dictionary<State, List<State>>();
        specification.Add(State.Idle, new List<State>() 
        { 
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.FollowPlayer,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Hide,
            State.Hibernate,
            State.Same
        });
        specification.Add(State.FollowPlayer, new List<State>() 
        { 
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Hide,
            State.Hibernate,
            State.Same
        });
        specification.Add(State.Hide, new List<State>() 
        { 
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.Same,
            State.SurprisePlayerFromBehind,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Hibernate,
            State.Same
        });
        specification.Add(State.SurprisePlayerFromBehind, new List<State>() 
        { 
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Idle, 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Hide,
            State.Hibernate,
            State.Same
        });
        stateMachine = new StateMachine(specification);
        specification.Add(State.Hibernate, new List<State>() 
        { 
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Same, 
            State.Same,
            State.Same,
            State.Same,
            State.Idle, 
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Same,
            State.Hide,
            State.Same,
            State.Same
        });

        spawnPosition = this.transform.position;
    }

    protected override void idle()
    {
        this.transform.position = spawnPosition;
        this.rigidbody.velocity = Vector3.zero;
        this.renderer.enabled = true;
        lookAtPlayer();
    }

    protected virtual void followPlayer()
    {
        moveTowardPlayer();
        if (Vector3.Distance(this.transform.position, gameManager.Player.transform.position) < 4f)
        {
            stateMachine.State = State.Hide;
            gameManager.HandleTriggerMessage(14);
        }
    }

    protected override void hide()
    {
        this.transform.position = spawnPosition;
        this.rigidbody.velocity = Vector3.zero;
        this.renderer.enabled = false;
    }

    protected virtual void surprisePlayerFromBehind()
    {
        if (renderer.enabled)
        {            
            if (Vector3.Distance(this.transform.position, gameManager.Player.transform.position) > 2f)
            {
                moveTowardPlayer();
            }
            else
            {
                this.rigidbody.velocity = Vector3.zero;
            }
            if (isPlayerLookingAtMe())
            {
                stateMachine.State = State.Hibernate;
                gameManager.HandleTriggerMessage(15);
            }
        }
        else
        {
            this.renderer.enabled = true;
            this.transform.position = spawnPosition;
        }        
    }

    protected virtual void hibernate()
    {
        hide();
    }

    protected virtual void moveTowardPlayer()
    {
        Vector3 direction = gameManager.Player.transform.position - this.transform.position;
        this.rigidbody.velocity = direction.normalized * MoveSpeed;
        lookAtPlayer();
    }

    protected virtual void lookAtPlayer()
    {
        this.transform.LookAt(gameManager.Player.transform, Vector3.up);
    }

    protected virtual bool isPlayerLookingAtMe()
    {
        Debug.DrawRay(Camera.main.transform.position, Camera.main.transform.forward);
        RaycastHit hit;
        if (Physics.Raycast(Camera.main.transform.position, Camera.main.transform.forward, out hit, 10f))
        {
            if (hit.collider.gameObject == this.gameObject)
            {
                return true;
            }
        }
        return false;
    }
}
