﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AStarPathManager : MonoBehaviour
{
    #region Class Members

    public float GridSize { get; set; }
    public bool IsCalculatingPath { get; protected set; }
    public Vector3 DesiredGoalPosition { get; protected set; }
    public Vector3 CurrentGoalPosition { get; protected set; }
    protected float nextSearchYieldTime = 0f;
    protected readonly float SEARCH_YIELD_TIME_INCREMENT= 0.02f;

    protected List<GameObject> obstacles;
    public List<GameObject> GetObstacles() 
    { 
        return obstacles; 
    }
    public void SetObstacles(string tag)
    {
        obstacles = new List<GameObject>(GameObject.FindGameObjectsWithTag(tag));
        obstacles.Add(GameObject.FindGameObjectWithTag("MainCamera"));
    }

    protected LinkedList<Vector3> path = new LinkedList<Vector3>();
    public LinkedList<Vector3> GetPath() 
    { 
        return path; 
    }

    #endregion

    #region Public Methods

    public IEnumerator FindPathTo(Vector3 desiredGoalPosition)
    {
        if (!IsCalculatingPath && this.DesiredGoalPosition != desiredGoalPosition)
        {
            CurrentGoalPosition = findValidGoalPosition(desiredGoalPosition);
            while (needNewPath())
            {
                if (!IsCalculatingPath)
                {
                    StartCoroutine(aStarPathSearch());
                }
                yield return null;
            }
            this.DesiredGoalPosition = desiredGoalPosition;
        }
        yield return 0;
    }

    public Vector3 GetNextPositionToMoveTo()
    {
        if (path.Count > 0)
        {
            if (isAtPosition(path.First.Value))
            {
                path.RemoveFirst();
            }
            if (path.Count > 0)
            {
                return path.First.Value;
            }
        }
        return this.transform.position;
    }

    #endregion

    #region Non-Public methods

    protected bool needNewPath()
    {
        if (path.Count <= 0 || path.Last.Value != CurrentGoalPosition)
        {
            return true;
        }
        return false;
    }

    protected IEnumerator aStarPathSearch()
    {
        IsCalculatingPath = true;
        nextSearchYieldTime = Time.time + SEARCH_YIELD_TIME_INCREMENT;

        SortedList<float, SearchNode> openNodeSet = new SortedList<float, SearchNode>();
        SearchNode rootNode = new SearchNode(this.transform.position, -1, 0.0f);
        openNodeSet.Add(Vector3.Distance(this.transform.position, CurrentGoalPosition), rootNode);

        SortedList<int, SearchNode> closedNodeSet = new SortedList<int, SearchNode>();

        List<SearchNode> childNodes = new List<SearchNode>();

        SearchNode currentNode;

        while (openNodeSet.Count > 0)
        {
            currentNode = openNodeSet.Values[0];
            openNodeSet.RemoveAt(0);
            int parentIndex = closedNodeSet.Count;
            closedNodeSet.Add(parentIndex, currentNode);

            float gValue = currentNode.GValue + GridSize;
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x + GridSize, currentNode.Cell.y, currentNode.Cell.z), parentIndex, gValue));
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x - GridSize, currentNode.Cell.y, currentNode.Cell.z), parentIndex, gValue));
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x, currentNode.Cell.y, currentNode.Cell.z + GridSize), parentIndex, gValue));
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x, currentNode.Cell.y, currentNode.Cell.z - GridSize), parentIndex, gValue));

            gValue = currentNode.GValue + (Mathf.Sqrt(2.0f) * GridSize);
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x + GridSize, currentNode.Cell.y, currentNode.Cell.z + GridSize), parentIndex, gValue));
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x - GridSize, currentNode.Cell.y, currentNode.Cell.z + GridSize), parentIndex, gValue));
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x + GridSize, currentNode.Cell.y, currentNode.Cell.z - GridSize), parentIndex, gValue));
            childNodes.Add(new SearchNode(new Vector3(currentNode.Cell.x - GridSize, currentNode.Cell.y, currentNode.Cell.z - GridSize), parentIndex, gValue));

            while (childNodes.Count > 0)
            {
                currentNode = childNodes[0];
                childNodes.RemoveAt(0);
                if (!isPointTooNearObstacle(currentNode.Cell))
                {
                    if (Vector3.Distance(currentNode.Cell, CurrentGoalPosition) <= GridSize)
                    {
                        LinkedList<Vector3> returnList = new LinkedList<Vector3>();
                        returnList.AddFirst(CurrentGoalPosition);
                        returnList.AddFirst(currentNode.Cell);

                        while (currentNode.Parent > -1)
                        {
                            if (!closedNodeSet.TryGetValue(currentNode.Parent, out currentNode))
                            {
                                IsCalculatingPath = false;
                                yield break;
                            }
                            returnList.AddFirst(currentNode.Cell);
                        }
                        IsCalculatingPath = false;
                        path = returnList;
                        yield break;
                    }
                    if (!isInList<float>(currentNode.Cell, openNodeSet) && !isInList<int>(currentNode.Cell, closedNodeSet))
                    {
                        gValue = currentNode.GValue + Vector3.Distance(currentNode.Cell, CurrentGoalPosition) + 0.1f * Random.value * Random.value;
                        try
                        {
                            openNodeSet.Add(gValue, currentNode);
                        }
                        catch (System.Exception e)
                        {
                            print(e.Message);
                            IsCalculatingPath = false;
                            yield break;
                        }
                    }
                }
                if (Time.time > nextSearchYieldTime)
                {
                    nextSearchYieldTime = Time.time + SEARCH_YIELD_TIME_INCREMENT;
                    yield return new WaitForSeconds(0.05f);
                }
            }
        }
        IsCalculatingPath = false;
        yield break;
    }

    protected bool isPointTooNearObstacle(Vector3 point)
    {
        Bounds pointBounds = new Bounds(point, new Vector3(GridSize, GridSize, GridSize));
        foreach (GameObject obstacle in obstacles)
        {
            Bounds obstacleBounds = obstacle.collider.bounds;
            if (obstacleBounds.Intersects(pointBounds))
            {
                return true;
            }
        }
        return false;
    }

    protected Vector3 findValidGoalPosition(Vector3 goalPosition)
    {
        goalPosition = new Vector3(goalPosition.x, this.transform.position.y, goalPosition.z);
        if (isPointTooNearObstacle(goalPosition))
        {
            List<Vector3> potentialPositions = new List<Vector3>();
            potentialPositions.Add(new Vector3(goalPosition.x + GridSize, goalPosition.y, goalPosition.z));
            potentialPositions.Add(new Vector3(goalPosition.x - GridSize, goalPosition.y, goalPosition.z));
            potentialPositions.Add(new Vector3(goalPosition.x, goalPosition.y, goalPosition.z + GridSize));
            potentialPositions.Add(new Vector3(goalPosition.x, goalPosition.y, goalPosition.z - GridSize));
            potentialPositions.Add(new Vector3(goalPosition.x + GridSize, goalPosition.y, goalPosition.z + GridSize));
            potentialPositions.Add(new Vector3(goalPosition.x - GridSize, goalPosition.y, goalPosition.z + GridSize));
            potentialPositions.Add(new Vector3(goalPosition.x + GridSize, goalPosition.y, goalPosition.z - GridSize));
            potentialPositions.Add(new Vector3(goalPosition.x - GridSize, goalPosition.y, goalPosition.z - GridSize));

            int attempts = 0;

            while (attempts < 100)
            {
                for (int i = 0; i < potentialPositions.Count; i++)
                {
                    if (!isPointTooNearObstacle(potentialPositions[i]))
                    {
                        return potentialPositions[i];
                    }
                }
                potentialPositions[0] = new Vector3(potentialPositions[0].x + GridSize, potentialPositions[0].y, potentialPositions[0].z);
                potentialPositions[1] = new Vector3(potentialPositions[1].x - GridSize, potentialPositions[1].y, potentialPositions[1].z);
                potentialPositions[2] = new Vector3(potentialPositions[2].x, potentialPositions[2].y, potentialPositions[2].z + GridSize);
                potentialPositions[3] = new Vector3(potentialPositions[3].x, potentialPositions[3].y, potentialPositions[3].z - GridSize);
                potentialPositions[4] = new Vector3(potentialPositions[4].x + GridSize, potentialPositions[4].y, potentialPositions[4].z + GridSize);
                potentialPositions[5] = new Vector3(potentialPositions[5].x - GridSize, potentialPositions[5].y, potentialPositions[5].z + GridSize);
                potentialPositions[6] = new Vector3(potentialPositions[6].x + GridSize, potentialPositions[6].y, potentialPositions[6].z + GridSize);
                potentialPositions[7] = new Vector3(potentialPositions[7].x - GridSize, potentialPositions[7].y, potentialPositions[7].z - GridSize);

                attempts++;
            }            
        }
        return goalPosition;
    }

    protected Vector3 getNearestGridCenter(Vector3 position)
    {
        float gridX = GridSize * Mathf.Round(position.x / GridSize);
        float gridZ = GridSize * Mathf.Round(position.z / GridSize);
        return new Vector3(gridX, position.y, gridZ);
    }

    protected bool isInList<T>(Vector3 position, SortedList<T, SearchNode> list)
    {
        foreach (KeyValuePair<T, SearchNode> element in list)
        {
            if (element.Value.Cell == position)
            {
                return true;
            }
        }
        return false;
    }

    protected bool isAtPosition(Vector3 goalPosition)
    {
        if (Vector3.Distance(this.transform.position, goalPosition) <= GridSize)
        {
            return true;
        }
        return false;
    }

    #endregion
}
