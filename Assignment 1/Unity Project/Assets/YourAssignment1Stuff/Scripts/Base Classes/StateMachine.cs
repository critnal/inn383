﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StateMachine
{
    public State State { get; set; }

    protected Dictionary<State, List<State>> specification;

    public StateMachine(Dictionary<State, List<State>> specification)
    {
        this.specification = specification;
        List<State> temp = new List<State>(specification.Keys);
        this.State = temp[0];
    }

    public void InputCharacter(int triggerID)
    {
        if (specification[State].Count > triggerID)
        {
            if (specification[State][triggerID] != State 
                && specification[State][triggerID] != State.Same)
            {
                //Debug.Log("State: " + State.ToString() + " new state: " + specification[State][triggerID]);
                State = specification[State][triggerID];
            }
        }
    }
}