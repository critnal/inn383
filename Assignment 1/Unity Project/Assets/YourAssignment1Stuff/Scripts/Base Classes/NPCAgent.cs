﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class NPCAgent : MonoBehaviour
{
    public float MoveSpeed { get; protected set; }
    public Vector3 PositionToMoveTo { get; protected set; }

    protected StateMachine stateMachine;
    protected Dictionary<State, Action> stateActions;


    protected GameManager gameManager;

    protected virtual void Start()
    {
        MoveSpeed = 5f;

        // For each of the agent's states
        // assign a method to be invoked
        stateActions = new Dictionary<State, Action>();
        //stateActions.Add(State.Idle, this.idle);
        //stateActions.Add(State.Follow, this.follow);

        // Create a specification for this agent's Finite State Machine
        //Dictionary<State, List<State>> specification = new Dictionary<State, List<State>>();

        // For each of the agent's implemented states
        // add its transition state
        // for each trigger
        //specification.Add(State.Idle, new List<State>() { State.Follow });
        //specification.Add(State.Follow, new List<State>() { State.Idle });

        //stateMachine = new StateMachine(specification);

        gameManager = GameObject.FindGameObjectWithTag("GameManager").GetComponent<GameManager>();
    }

    protected virtual void Update()
    {
        // Invoke the method that is assigned to the Agent's current state
        stateActions[stateMachine.State].Invoke();
    }

    protected virtual void FixedUpdate() { }

    #region States and Actions

    public void HandleTriggerMessage(int triggerID)
    {
        stateMachine.InputCharacter(triggerID);
    }

    public virtual void HandleTriggerMessage(State triggerMessage)
    {
        stateMachine.State = triggerMessage;
    }

    public void SetState(State state)
    {
        stateMachine.State = state;
    }

    protected virtual void idle()
    {
        this.rigidbody.velocity = Vector3.zero;
    }

    protected virtual void patrol() { }

    protected virtual void hide() { }

    protected virtual void follow()
    {
        print("follow");
    }

    #endregion

    #region Movement

    public void MoveInDirection(Vector3 direction)
    {
        this.rigidbody.velocity = direction.normalized * MoveSpeed;
    }

    public void MoveToward(Vector3 targetPosition)
    {
        this.rigidbody.velocity = Vector3.Normalize(targetPosition - this.transform.position) * MoveSpeed;
    }

    public void HaltMovement()
    {
        this.rigidbody.velocity = new Vector3(0f, this.rigidbody.velocity.y, 0f);
    }

    #endregion 
}
