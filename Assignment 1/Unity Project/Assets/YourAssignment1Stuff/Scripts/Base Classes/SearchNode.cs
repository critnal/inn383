﻿using UnityEngine;
using System.Collections;

public struct SearchNode
{
    public Vector3 Cell;
    public int Parent;
    public float GValue;

    public SearchNode(Vector3 cell, int parent, float gValue)
    {
        this.Cell = cell;
        this.Parent = parent;
        this.GValue = gValue;
    }
}
