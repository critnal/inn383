﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathNPCAgent : NPCAgent
{
    protected AStarPathManager pathManager;

    protected override void Start()
    {
        base.Start();
        pathManager = this.gameObject.AddComponent<AStarPathManager>();
        pathManager.GridSize = this.collider.bounds.size.x / 2;
        pathManager.SetObstacles("PathObstacle");
    }

    protected override void Update()
    {
        base.Update();
    }

    public void CalculatePathTo(Vector3 position)
    {
        StartCoroutine(pathManager.FindPathTo(position));
    }

    protected override void idle()
    {
        followPath();
    }

    protected virtual void followPath()
    {
        if (pathManager.GetPath().Count > 0)
        {
            PositionToMoveTo = pathManager.GetNextPositionToMoveTo();
            MoveToward(PositionToMoveTo);
        }
        else
        {
            HaltMovement();
        }
    }
}
