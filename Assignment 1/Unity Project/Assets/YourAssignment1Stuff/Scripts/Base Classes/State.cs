﻿using UnityEngine;
using System.Collections;

public enum State
{
    Same,
    Idle,
    Patrol,
    Hide,
    Follow,
    FollowPlayer,
    ReturnToStartRoom,
    PlayerLeftClicked,
    MoveToPlayerLeftClickPosition,
    Bounce,
    Freeze,
    BounceCloseToPlayer,
    BounceAwayFromPlayer,
    Hibernate,
    SurprisePlayerFromBehind
}
