﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public sealed class GameManager : MonoBehaviour
{
    public GameObject Player { get; private set; }
    public Vector3 PlayerLeftClickPosition { get; private set; }

    private List<NPCAgent> npcAgents;
    private PlayerBall playerBall;

    private void Start()
    {
        Player = GameObject.FindGameObjectWithTag("MainCamera");
        PlayerLeftClickPosition = Player.transform.position;

        // Get a reference to every NPCAgent in the scene so they can all
        // be easily notified of trigger messages
        npcAgents = new List<NPCAgent>();
        GameObject[] gameObjects = GameObject.FindGameObjectsWithTag("NPCAgent");
        foreach (GameObject gameObject in gameObjects)
        {
            if (gameObject.GetComponent<NPCAgent>() != null)
            {
                npcAgents.Add(gameObject.GetComponent<NPCAgent>());
            }
            else
            {
                print("null");
            }
        }
        playerBall = GameObject.Find("PlayerBall").GetComponent<PlayerBall>();
    }

    private void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            RaycastHit rayCastHit;
            if (Physics.Raycast(ray, out rayCastHit))
            {
                PlayerLeftClickPosition = rayCastHit.point;
                playerBall.HandleTriggerMessage(16);
            }

        }
    }

    public void HandleTriggerMessage(int trigger)
    {
        foreach (NPCAgent npcAgent in npcAgents) 
        {
            npcAgent.HandleTriggerMessage(trigger);
        }
    }
}
