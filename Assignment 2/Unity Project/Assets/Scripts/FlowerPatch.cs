﻿using UnityEngine;
using System.Collections;

public class FlowerPatch : MonoBehaviour
{
    protected FitnessModel fitnessModel;

    protected void Start()
    {
        fitnessModel = new FitnessModel
        (
            0f, 
            Simulation.GetRandomInitialFlowerPatchFitness(),
            Simulation.FlowerPatchTopUpRate
        );
    }

    protected void Update()
    {
        fitnessModel.Update(Time.deltaTime);
    }

    public void Collect(float depletionAmount)
    {
        fitnessModel.Take(depletionAmount);
    }

    public float GetFitness()
    {
        return fitnessModel.Fitness;
    }

    public float GetMaxFitness()
    {
        return fitnessModel.Max;
    }
}
