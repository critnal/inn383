﻿using UnityEngine;
using System.Collections;

public enum BeeTrigger
{
    Idle,
    Scout,
    Collect,
    Return
}
