﻿using UnityEngine;
using System.Collections;

public class Boid : MonoBehaviour
{
    protected Vector3 flyZoneCentre = Vector3.zero;
    protected float flyZoneRadius = 20f;

    protected float acceleration = 15f;
    protected float angularAcceleration = 5f;
    protected float maxSpeed = 3f;
    protected float viewingAngle = 110;
    protected float neighbourhoodDistance = 7f;
    
    protected float separationStrength = 5f;

    protected float alignmentStrength = 2f;

    protected float cohesionStrength = 1f;
    protected Vector3 cohesionPositionSum = Vector3.zero;
    protected int neighbourCount = 0;

    protected float wanderCircleRadius = 1f;
    protected float wanderCircleDistance = 2f;
    protected float wanderAngleChange = 1f;

    protected Boid[] boids = null;
    protected int boidIndex = 0;
    protected bool isFlocking = false;

    protected virtual void Start() { }

    protected virtual void Update()
    {
        applyBoid();
    }

    protected void applyBoid()
    {
        applyBehaviourForces();

        applyNormalMovement();
    }

    #region Behaviour Forces

    protected virtual void applyBehaviourForces()
    {
        if (boids == null)
        {
            boids = GameObject.FindObjectsOfType<Boid>();
        }

        if (boids[boidIndex] != this)
        {
            Vector3 otherBoidPosition = boids[boidIndex].transform.position;
            float distanceFromOtherBoid = Vector3.Distance(transform.position, otherBoidPosition);

            if (isInNeighbourhood(distanceFromOtherBoid, otherBoidPosition))
            {
                isFlocking = true;
                Quaternion otherBoidRotation = boids[boidIndex].transform.rotation;

                applySeparationForce(distanceFromOtherBoid, otherBoidPosition);                
                applyAlignmentForce(otherBoidRotation);
                updateCohesionPosition(otherBoidPosition);
            }
        }

        boidIndex++;
        if (boidIndex >= boids.Length)
        {
            applyCohesionForce();
            boidIndex = 0;
            isFlocking = false;
        }

        if (!isFlocking)
        {
            wander();
        }
    }

    protected bool isInNeighbourhood(float distanceFromOtherBoid, Vector3 otherBoidPosition)
    {
        if (distanceFromOtherBoid > neighbourhoodDistance)
        {
            return false;
        }
        float angleToOtherBoid 
            = Vector3.Angle(otherBoidPosition - transform.position, this.transform.forward);
        return angleToOtherBoid < viewingAngle;
    }

    protected void applySeparationForce(float distanceFromOtherBoid, Vector3 otherBoidPosition)
    {
        float separationScale = separationStrength / Mathf.Pow(distanceFromOtherBoid, 2);
        rigidbody.AddForce(separationScale * Vector3.Normalize(transform.position - otherBoidPosition));
    }

    protected void applyAlignmentForce(Quaternion otherBoidRotation)
    {
        transform.rotation = Quaternion.RotateTowards(transform.rotation, otherBoidRotation, alignmentStrength);
    }

    protected void updateCohesionPosition(Vector3 otherBoidPosition)
    {
        neighbourCount++;
        cohesionPositionSum += otherBoidPosition;
    }

    protected void applyCohesionForce()
    {
        if (neighbourCount > 0)
        {
            Vector3 cohesionPositionAverage = cohesionPositionSum / neighbourCount;
            Vector3 cohesionDirection = cohesionPositionAverage - transform.position;
            Vector3 cohesionForce = cohesionStrength * cohesionDirection;
            rigidbody.AddForce(cohesionForce);
        }
        neighbourCount = 0;
        cohesionPositionSum = transform.position;
    }

    protected void wander()
    {
        float angle = Random.Range(0f, Mathf.PI * 2f);
        Vector3 wanderDirection = new Vector3(Mathf.Cos(angle) * wanderCircleRadius, 0f, Mathf.Sin(angle) * wanderCircleRadius);
        Vector3 targetDirection = Vector3.RotateTowards(transform.forward, wanderDirection, 5f * Time.deltaTime, 0f);
        transform.rotation = Quaternion.LookRotation(targetDirection);
    }

    #endregion

    protected void applyNormalMovement()
    {
        if (isTooFarFromSimulationCentre())
        {
            steerToward(flyZoneCentre);
        }

        rigidbody.AddForce(transform.forward * acceleration);
        if (rigidbody.velocity.magnitude > maxSpeed)
        {
            rigidbody.velocity = rigidbody.velocity.normalized * maxSpeed;
        }
    }

    protected bool isTooFarFromSimulationCentre()
    {
        return Vector3.Distance(transform.position, flyZoneCentre) > flyZoneRadius;
    }

    protected void steerToward(Vector3 goal)
    {
        float step = angularAcceleration * Time.deltaTime;
        Vector3 targetPosition = goal - transform.position;
        Vector3 targetDirection = Vector3.RotateTowards(transform.forward, targetPosition, step, 0f);
        transform.rotation = Quaternion.LookRotation(targetDirection);
    }
}