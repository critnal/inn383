﻿using UnityEngine;
using System.Collections;

public enum BeeState
{
    Idle,
    Scout,
    Collect,
    Return
}
