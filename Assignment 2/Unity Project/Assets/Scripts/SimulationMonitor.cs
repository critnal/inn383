﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SimulationMonitor : MonoBehaviour
{
    protected Hive hive;
    protected FlowerPatch[] flowerPatches;

    protected GUIStyle labelStyle = new GUIStyle();

    protected void Start()
    {
        hive = GameObject.FindObjectOfType<Hive>();

        labelStyle.normal.textColor = Color.green;
    }

    protected void Update()
    {
        if (flowerPatches == null)
        {
            flowerPatches = GameObject.FindObjectsOfType<FlowerPatch>();
        }
        labelStyle.fontSize = Mathf.RoundToInt(Screen.height / 50f);
    }

    protected void OnGUI()
    {
        Rect patchInfoRect = new Rect(0f, 0f, Screen.width, Screen.height);
        GUILayout.BeginArea(patchInfoRect);
        {
            labelStyle.alignment = TextAnchor.UpperLeft;
            GUILayout.Label("All Patches:", labelStyle);
            for (int i = 0; i < flowerPatches.Length; i++)
            {
                GUILayout.Label
                (
                    string.Format("#{0} - {1:0} / {2:0}", i, flowerPatches[i].GetFitness(), flowerPatches[i].GetMaxFitness()), 
                    labelStyle
                );
            }            
        }
        GUILayout.EndArea();

        List<TrackedFlowerPatch> trackedPatches = hive.GetTrackedFlowerPatches();
        for (int i = 0; i < trackedPatches.Count; i++)
        {
            for (int j = 0; j < flowerPatches.Length; j++)
            {
                if (trackedPatches[i].Location == flowerPatches[j].gameObject.transform.position)
                {
                    Vector3 position = Camera.main.WorldToScreenPoint(trackedPatches[i].Location);
                    labelStyle.alignment = TextAnchor.UpperCenter;
                    GUI.Label
                    (
                        new Rect(position.x - 20f, Screen.height - position.y - 10f, 40f, 40f),
                        string.Format("#{0} - {1:0}", j, trackedPatches[i].Fitness),
                        labelStyle
                    );
                }
            }
        }
    }
}
