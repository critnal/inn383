﻿using UnityEngine;
using System.Collections;

public class TrackedFlowerPatch
{
    public Vector3 Location { get; protected set; }
    public float Fitness { get; set; }

    public TrackedFlowerPatch(Vector3 location, float fitness)
    {
        this.Location = location;
        this.Fitness = fitness;
    }
}
