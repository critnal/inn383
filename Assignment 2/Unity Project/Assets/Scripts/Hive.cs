﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System;

public class Hive : MonoBehaviour
{
    public Transform ScoutBee;
    public Transform CollectorBee;

    protected List<Bee> scouts = new List<Bee>();
    protected List<Bee> collectors = new List<Bee>();
    protected List<TrackedFlowerPatch> trackedPatches = new List<TrackedFlowerPatch>();
    protected List<TrackedFlowerPatch> assignedPatches = new List<TrackedFlowerPatch>();

    #region Setup

    void Start()
    {
        this.transform.position = Simulation.SimulationCentre;

        spawnBeeGroup(scouts, Simulation.NumberOfScoutBees, ScoutBee);
        spawnBeeGroup(collectors, Simulation.NumberOfCollectorBees, CollectorBee);
    }

    protected void spawnBeeGroup(List<Bee> group, int number, Transform prefab)
    {
        for (int i = 0; i < number; i++)
        {
            Transform beeObject = (Transform)Instantiate
            (
                prefab,
                new Vector3
                (
                    UnityEngine.Random.Range(-Simulation.SimulationRadius, Simulation.SimulationRadius) / 2f,
                    this.transform.position.y,
                    UnityEngine.Random.Range(-Simulation.SimulationRadius, Simulation.SimulationRadius) / 2f
                ),
                Quaternion.identity
            );
            Bee bee = beeObject.GetComponent<Bee>();
            group.Add(bee);
        }
    }

    #endregion

    #region Flower Patch Management

    public void ReportFlowerPatch(TrackedFlowerPatch patch)
    {
        if (trackedPatches.Any(p => p.Location == patch.Location))
        {
            trackedPatches.Single(p => p.Location == patch.Location).Fitness = patch.Fitness;
        }
        else
        {
            trackedPatches.Add(new TrackedFlowerPatch(patch.Location, patch.Fitness));
        }

        updatePatchBeeAllocations();
    }

    protected void updatePatchBeeAllocations()
    {
        if (trackedPatches.Count > 0)
        {
            float sumOfPatchRatings = trackedPatches.Sum
            (
                p => getPatchRating(p)
            );

            float ratingCount = 0f;
            int patchIndex = 0;
            assignedPatches.Clear();
            for (int i = 0; i < collectors.Count; i++)
            {
                assignedPatches.Add(trackedPatches[patchIndex]);

                float currentRating = getPatchRating(trackedPatches[patchIndex]);
                if (i > ((ratingCount + currentRating) / sumOfPatchRatings * collectors.Count))
                {
                    ratingCount += currentRating;
                    patchIndex++;
                }
            }
        }
    }

    protected float getPatchRating(TrackedFlowerPatch patch)
    {
        return patch.Fitness / Vector3.Distance(this.transform.position, patch.Location);
    }

    public List<TrackedFlowerPatch> GetTrackedFlowerPatches()
    {
        return trackedPatches;
    }

    public Vector3 GetAssignedPatchLocation(Bee bee)
    {
        if (trackedPatches.Count > 0)
        {
            for (int i = 0; i < collectors.Count; i++)
            {
                if (bee == collectors[i])
                {
                    return assignedPatches[i].Location;
                }
            }
        }
        return Simulation.NullVector;
    }

    #endregion
}