﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StateMachine<S, T>
{
    public S State { get; protected set; }

    protected Dictionary<S, Dictionary<T, S>> specification;

    public StateMachine(Dictionary<S, Dictionary<T, S>> specification, S initialState) 
    {
        this.specification = specification;
        this.State = initialState;
    }

    public void Transition(T trigger)
    {
        State = specification[State][trigger];
    }
}
