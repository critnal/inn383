﻿using System.Collections;
using System;

public class FitnessModel
{
    public float Min { get; set; }
    public float Max { get; set; }
    public float TopUpRate { get; set; }
    public float Fitness { get; protected set; }

    public FitnessModel(float min, float max, float topUpRate)
    {
        this.Min = min;
        this.Max = max;
        this.TopUpRate = topUpRate;
    }

    public void Update(float deltaTime)
    {
        TopUp(deltaTime);
    }

    protected void TopUp(float deltaTime)
    {
        Fitness = Math.Min(Max, Fitness + TopUpRate * deltaTime);
    }

    public float Take(float amountToTake)
    {
        float maxTakePossible = Math.Min(amountToTake, Fitness);
        Fitness = Math.Max(Fitness - amountToTake, 0f);
        return maxTakePossible;
    }
}
