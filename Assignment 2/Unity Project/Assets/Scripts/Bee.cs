﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Bee : Boid
{
    protected Hive hive;
    protected StateMachine<BeeState, BeeTrigger> stateMachine;
    public Vector3 AssignedPatchLocation;


    protected override void Start()
    {
        base.Start();

        hive = GameObject.FindObjectOfType<Hive>();
        flyZoneCentre = Simulation.SimulationCentre;
        flyZoneRadius = Simulation.SimulationRadius;
    }

    protected void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<FlowerPatch>() != null)
        {
            FlowerPatch patch = other.GetComponent<FlowerPatch>();
            visitFlowerPatch(patch);
        }
        else if (other.GetComponent<Hive>() != null && other.GetComponent<Hive>() == hive)
        {
            enterHive();
        }
    }

    protected virtual void visitFlowerPatch(FlowerPatch patch) { }

    protected virtual void enterHive() { }
}