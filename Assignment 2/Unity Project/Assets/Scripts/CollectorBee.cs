﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CollectorBee : Bee
{
    protected float idleRadius = Simulation.SimulationRadius / 7f;

    protected override void Start()
    {
        base.Start();

        stateMachine = new StateMachine<BeeState, BeeTrigger>
        (
            new Dictionary<BeeState, Dictionary<BeeTrigger, BeeState>> 
            {
                {
                    BeeState.Idle, new Dictionary<BeeTrigger, BeeState> 
                    {
                        { BeeTrigger.Idle, BeeState.Idle },
                        { BeeTrigger.Scout, BeeState.Idle },
                        { BeeTrigger.Collect, BeeState.Collect },
                        { BeeTrigger.Return, BeeState.Idle }
                    }
                },
                {
                    BeeState.Collect, new Dictionary<BeeTrigger, BeeState> 
                    {
                        { BeeTrigger.Idle, BeeState.Idle },
                        { BeeTrigger.Scout, BeeState.Idle },
                        { BeeTrigger.Collect, BeeState.Collect },
                        { BeeTrigger.Return, BeeState.Return }
                    }
                },
                {
                    BeeState.Return, new Dictionary<BeeTrigger, BeeState> 
                    {
                        { BeeTrigger.Idle, BeeState.Idle },
                        { BeeTrigger.Scout, BeeState.Idle },
                        { BeeTrigger.Collect, BeeState.Collect },
                        { BeeTrigger.Return, BeeState.Return }
                    }
                }        
            },
            BeeState.Return
        );

        AssignedPatchLocation = Simulation.NullVector;
    }

    protected override void Update()
    {
        switch (stateMachine.State)
        {
            case BeeState.Idle :
                flyZoneRadius = idleRadius;
                break;

            case BeeState.Collect :
                flyZoneRadius = Simulation.SimulationRadius;
                steerToward(AssignedPatchLocation);
                break;

            case BeeState.Return :
                steerToward(hive.transform.position);
                break;

            default : 
                break;
        }

        applyBoid();
    }

    #region Collector Functions

    protected override void visitFlowerPatch(FlowerPatch patch)
    {
        if (patch.transform.position == AssignedPatchLocation)
        {
            patch.Collect(Simulation.BeeCollectAmount);
            stateMachine.Transition(BeeTrigger.Return);
        }
    }

    protected override void enterHive() 
    {
        AssignedPatchLocation = hive.GetAssignedPatchLocation(this);

        if (AssignedPatchLocation != Simulation.NullVector)
        {
            stateMachine.Transition(BeeTrigger.Collect);
        }
        else
        {
            stateMachine.Transition(BeeTrigger.Idle);
        }
    }

    #endregion
}
