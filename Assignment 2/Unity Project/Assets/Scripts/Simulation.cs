﻿using UnityEngine;
using System.Collections;

public class Simulation : MonoBehaviour
{
    public Transform FlowerPatch;

    public static readonly Vector3 SimulationCentre = Vector3.zero;
    public static readonly float SimulationRadius = 20;
    public static readonly Vector3 NullVector = new Vector3(999f, 999f, 999f);

    public static readonly float MinFlowerPatchSpawnRadius = 5;
    public static readonly int MaxFlowerPatches = 5;
    public static readonly float MinInitialFlowerPatchFitness = 100;
    public static readonly float MaxInitialFlowerPatchFitness = 1000;
    public static readonly float FlowerPatchTopUpRate = 2;

    public static readonly int NumberOfScoutBees = 4;
    public static readonly int NumberOfCollectorBees = 16;
    public static readonly float BeeCollectAmount = 4;
    public static readonly float BeeScoutRadiusDecayRate = 1.16158634964f;

    public static float GetRandomInitialFlowerPatchFitness()
    {
        return Random.Range(MinInitialFlowerPatchFitness, MaxInitialFlowerPatchFitness);
    }

    protected void Start()
    {
        for (int i = 0; i < Simulation.MaxFlowerPatches; i++)
        {
            Transform patch = (Transform)Instantiate
            (
                FlowerPatch,
                Vector3.zero,
                Quaternion.Euler(0f, Random.Range(0f, 360f), 0f)
            );
            while (Vector3.Distance(patch.transform.position, SimulationCentre) < MinFlowerPatchSpawnRadius)
            {
                patch.transform.position = SimulationCentre + new Vector3
                (
                    Random.Range(-Simulation.SimulationRadius * 0.75f, Simulation.SimulationRadius * 0.75f),
                    SimulationCentre.y,
                    Random.Range(-Simulation.SimulationRadius * 0.75f, Simulation.SimulationRadius * 0.75f)
                );
            }
        }
    }
}
