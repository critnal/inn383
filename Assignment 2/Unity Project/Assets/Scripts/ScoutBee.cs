﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ScoutBee : Bee
{
    protected List<TrackedFlowerPatch> trackedFlowerPatches = new List<TrackedFlowerPatch>();

    protected float scoutMaxRadius = Simulation.SimulationRadius;
    protected float scoutRadiusDecayRate = Simulation.BeeScoutRadiusDecayRate;
    protected float scoutRadiusDecayStartTime = 0f;

    protected override void Start()
    {
        base.Start();

        stateMachine = new StateMachine<BeeState, BeeTrigger>
        (
           new Dictionary<BeeState, Dictionary<BeeTrigger, BeeState>> 
            {                
                {
                    BeeState.Scout, new Dictionary<BeeTrigger, BeeState> 
                    {
                        { BeeTrigger.Idle, BeeState.Scout },
                        { BeeTrigger.Scout, BeeState.Scout },
                        { BeeTrigger.Collect, BeeState.Scout },
                        { BeeTrigger.Return, BeeState.Scout }
                    }
                }        
            },
           BeeState.Scout
        );
    }

    protected override void Update()
    {
        switch (stateMachine.State)
        {
            case BeeState.Scout :
                decayScoutRadius();
                applyNormalMovement();
                wander();
                break;

            default :
                break;
        }


    }

    #region Scout Functions

    protected void decayScoutRadius()
    {
        float currentRadius = scoutMaxRadius - Mathf.Pow(scoutRadiusDecayRate, (Time.time - scoutRadiusDecayStartTime));
        flyZoneRadius = Mathf.Max(0f, currentRadius);
    }

    protected void resetScoutRadius()
    {
        scoutRadiusDecayStartTime = Time.time;
        flyZoneRadius = Simulation.SimulationRadius;
    }

    #endregion

    #region Flower Patch Functions

    protected override void visitFlowerPatch(FlowerPatch patch)
    {
        trackFlowerPatch(patch);
    }

    protected void trackFlowerPatch(FlowerPatch patch)
    {
        foreach (TrackedFlowerPatch trackedPatch in trackedFlowerPatches)
        {
            if (trackedPatch.Location == patch.transform.position)
            {
                trackedPatch.Fitness = patch.GetFitness();
                return;
            }
        }

        trackedFlowerPatches.Add(new TrackedFlowerPatch(patch.transform.position, patch.GetFitness()));
    }

    #endregion

    #region Hive Functions

    protected override void enterHive()
    {
        reportTrackedPatches();

        resetScoutRadius();
    }

    protected void reportTrackedPatches()
    {
        foreach (TrackedFlowerPatch trackedPatch in trackedFlowerPatches)
        {
            hive.ReportFlowerPatch(trackedPatch);
        }
        trackedFlowerPatches.Clear();
    }

    #endregion
}
